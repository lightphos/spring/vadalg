package uk.co.ac.vadalg;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Map;

@Slf4j
@RestController
@SpringBootApplication
public class KnEventListener {

    public static void main(String[] args) {
        SpringApplication.run(KnEventListener.class, args);
    }

    @GetMapping(value = "/", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> status() {
        return ResponseEntity.ok("{\"status\": \"UP\"}");
    }


    @PostMapping(value = "/", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Void> event(@RequestHeader Map<String, Object> headers, @RequestBody String body) throws Exception {
        log.info(LocalDateTime.now() + ", " + body);
        return ResponseEntity.accepted().build();
    }

}
