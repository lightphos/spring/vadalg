package uk.co.ac.vadalg;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Slf4j
@RestController
@SpringBootApplication
public class KnEventSender {

    public static void main(String[] args) {
        SpringApplication.run(KnEventSender.class, args);
    }

    @Value("${ksink}")
    private String sink;

    @PostConstruct
    public void fire() {
        try {
            log.info("Fire {} ", send());
            System.exit(0);
        }
        catch (Exception e ) {
            log.error("sink {}", sink);
            log.error("error ", e);
        }
    }

    @GetMapping(value = "/", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> send() {
        log.info("Sink {}", sink);
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>("{ 'test': 'from kn sender " + Math.random() + "'}", headers);

        return restTemplate.postForEntity(sink, request, String.class);
    }

}
